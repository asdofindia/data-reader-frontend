const parseMetadata = JSON.parse

const onMetadataChanged = (event) => {
    const text = event.target.value
    respondToMetadata(text)
}

const respondToMetadata = (text) => {
    if (text !== "") {
        try {
            const parsed = parseMetadata(text)
            const element = createFields(parsed.fields)
            replaceResultWithElement(element)
        } catch (err) {
            replaceResult(err)
        }
    }
}

const createFields = (fields) => {
    const wrapperOfFields = document.createElement('div')
    const fieldElements = fields.map(createField)

    for (const field of fields) {
        wrapperOfFields.appendChild(createField(field))
        wrapperOfFields.appendChild(createFieldChild('validation', validateField(field)))
    }

    return wrapperOfFields
}

const createField = ({field, range, pattern, value}) => {
    const fieldWrapper = document.createElement('div')
    fieldWrapper.setAttribute('class', 'field')

    fieldWrapper.appendChild(createFieldChild('name', field))
    fieldWrapper.appendChild(createFieldChild('range', range))
    fieldWrapper.appendChild(createFieldChild('pattern', pattern))
    fieldWrapper.appendChild(createFieldChild('value', value))

    return fieldWrapper
}

const validateField = (field) => {
    let result = ''
    if ('field' in field) {
        result += `Field ${field.field} identified. It is ${fieldNameValidity(field.field)} field`
    }
    return result
}

const fieldNameValidity = (fieldName) => {
    const validNames = ["entity.state", "entity.country", "entity.district", "year", "duration.start", "duration.end", "gender", "settlement", "value", "indicator"]
    if (validNames.includes(fieldName)) return "valid"
    return "INVALID"
}

const createFieldChild = (name, value) => {
    const fieldChild = document.createElement('div')
    fieldChild.setAttribute('class', name)
    fieldChild.textContent = value
    return fieldChild
}

const resultElement = document.querySelector("#result")

const replaceResult = (result) => {
    resultElement.innerText = result
}

const replaceResultWithElement = (result) => {
    resultElement.innerHTML = ''
    resultElement.appendChild(result)
}

const metadataElement = document.querySelector("#metadataText")

metadataElement.addEventListener('input', onMetadataChanged)

respondToMetadata(metadataElement.value)
